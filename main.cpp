#include <boost/asio.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/thread.hpp>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <thread>

namespace asio = boost::asio;
namespace posix = boost::asio::posix;

using boost::system::error_code;

auto constexpr static default_device = "/dev/ttyUARTPL2303";

using read_state = std::array<unsigned char, 1>;
using circular_buffer = boost::circular_buffer<int>;

const int BTN_1[] = {110, 118, 111, 231, 246, 246};
const int BTN_2[] = {110, 118, 111, 230, 238, 246};
const int BTN_3[] = {110, 118, 111, 111, 230, 246};
const int BTN_4[] = {110, 118, 111, 238, 110, 246};
const int BTN_5[] = {110, 118, 111, 119, 102, 246};
const int BTN_6[] = {110, 118, 111, 118, 118, 246};
const int BTN_7[] = {110, 118, 111, 111, 102, 246};
const int BTN_8[] = {110, 118, 111, 246, 110, 110, 255};
const int BTN_9[] = {110, 118, 111, 103, 102, 110, 255};
const int BTN_0[] = {110, 118, 111, 102, 246, 246};

const int BTN_POWER[] = {110, 118, 111, 118, 246, 111, 255};
const int BTN_BOOK[] = {110, 118, 111, 102, 239, 103, 255};
const int BTN_RATIO[] = {110, 118, 111, 103, 111, 103, 255};
const int BTN_INPUT[] = {110, 118, 111, 239, 230, 111, 255};
const int BTN_TV[] = {110, 118, 111, 102, 111, 111, 255};
const int BTN_LIST[] = {110, 118, 111, 111, 102, 119, 255};
const int BTN_QVIEW[] = {110, 118, 111, 102, 118, 110, 255};
const int BTN_FAV[] = {110, 118, 111, 246, 118, 246};
const int BTN_3D[] = {110, 118, 111, 110, 246, 118};
const int BTN_MUTE[] = {110, 118, 111, 103, 246, 111, 255};
const int BTN_INFO[] = {110, 118, 111, 102, 118, 102};
const int BTN_SMART[] = {110, 118, 111, 110, 239, 230};
const int BTN_APPS[] = {110, 118, 111, 102, 102, 239};
const int BTN_OK[] = {110, 118, 111, 110, 118, 239};
const int BTN_BACK[] = {110, 118, 111, 118, 110, 102, 255};
const int BTN_GUIDE[] = {110, 118, 111, 239, 119, 102};
const int BTN_EXIT[] = {110, 118, 111, 239, 102, 238};
const int BTN_UP[] = {110, 118, 111, 102, 246, 239};
const int BTN_DOWN[] = {110, 118, 111, 103, 110, 239};
const int BTN_LEFT[] = {255, 255, 255, 255,
                        255, 255, 255}; // why the same with volume down ?
const int BTN_RIGHT[] = {110, 118, 111, 118, 110, 111, 255};

const int BTN_VOL_UP[] = {110, 118, 111, 102, 102, 111, 255};
const int BTN_VOL_DOWN[] = {110, 118, 111, 111, 102, 111, 255};
const int BTN_PAGE_UP[] = {110, 118, 111, 102, 111, 111, 255};
const int BTN_PAGE_DOWN[] = {110, 118, 111, 103, 110, 111, 255};

const int BTN_DOT1[] = {110, 118, 111, 230, 119, 231};
const int BTN_DOT2[] = {110, 118, 111, 231, 103, 231};
const int BTN_DOT3[] = {110, 118, 111, 111, 231, 103};
const int BTN_DOT4[] = {110, 118, 111, 103, 103, 111, 255};

const int BTN_TEXT[] = {110, 118, 111, 230, 246, 247};
const int BTN_TOPT[] = {110, 118, 111, 103, 246, 247};
const int BTN_QMENU[] = {110, 118, 111, 119, 103, 239};
const int BTN_PLAY[] = {110, 118, 111, 102, 246, 103};
const int BTN_PAUSE[] = {110, 118, 111, 102, 247, 231, 255};
const int BTN_STOP[] = {110, 118, 111, 231, 102, 103};
const int BTN_REWIND[] = {110, 118, 111, 111, 118, 246, 255};
const int BTN_FORWARD[] = {110, 118, 111, 246, 110, 246, 255};
const int BTN_REC[] = {110, 118, 111, 247, 119, 102};
const int BTN_SETTINGS[] = {110, 118, 111, 111, 231, 239};
const int BTN_AVMODE[] = {110, 118, 111, 102, 110, 103, 255};
const int BTN_SIMPLINK[] = {110, 118, 111, 246, 239, 230};

const int SIZEOF_BTN_GROUP1 = sizeof(BTN_1) / sizeof(BTN_1[0]);
const int SIZEOF_BTN_GROUP2 = sizeof(BTN_VOL_UP) / sizeof(BTN_VOL_UP[0]);

void inline trace(const char *msg) {
  std::cout << std::chrono::steady_clock::now().time_since_epoch().count()
            << msg << std::endl;
}

bool compareCircularBufferToConstArray(
    const boost::circular_buffer<int> &buffer, const int *constArray,
    int constArraySize) {
  // If the sizes are different, the circular buffer and the array are not equal
  if (buffer.size() != constArraySize) {
    return false;
  }

  // Compare the elements of the circular buffer to the constant array
  auto it = buffer.begin();
  for (int i = 0; it != buffer.end(); ++it, ++i) {
    if (*it != constArray[i]) {
      return false; // Elements at the current position are not equal
    }
  }

  return true; // All elements are equal
}

void inline matchButtons(std::shared_ptr<circular_buffer> cb_ptr) {
  if (compareCircularBufferToConstArray(*cb_ptr, BTN_VOL_UP,
                                        SIZEOF_BTN_GROUP2)) {
    trace(" : VOLUME UP");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_VOL_DOWN,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : VOLUME DOWN");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_1,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 1");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_2,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 2");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_3,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 3");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_4,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 4");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_5,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 5");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_6,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 6");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_7,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 7");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_8,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : 8");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_9,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : 9");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_0,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 0");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_POWER,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : POWER");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_BOOK,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : BOOK");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_RATIO,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : RATIO");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_INPUT,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : INPUT");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_TV,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : TV");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_LIST,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : LIST");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_QVIEW,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : QVIEW");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_FAV,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : FAV");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_3D,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : 3D");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_MUTE,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : MUTE");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_INFO,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : INFO");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_SMART,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : SMART");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_APPS,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : APPS");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_OK,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : OK");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_BACK,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : BACK");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_GUIDE,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : GUIDE");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_EXIT,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : EXIT");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_UP,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : UP");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_DOWN,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : DOWN");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_RIGHT,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : RIGHT");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_DOT1,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : DOT1");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_DOT2,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : DOT2");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_DOT3,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : DOT3");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_DOT4,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : DOT4");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_TEXT,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : TEXT");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_TOPT,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : TOPT");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_QMENU,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : QMENU");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_STOP,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : STOP");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_PLAY,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : PLAY");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_PAUSE,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : PAUSE");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_REWIND,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : REWIND");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_FORWARD,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : FORWARD");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_REC,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : REC");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_SETTINGS,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : SETTINGS");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_AVMODE,
                                               SIZEOF_BTN_GROUP2)) {
    trace(" : AVMODE");
  } else if (compareCircularBufferToConstArray(*cb_ptr, BTN_SIMPLINK,
                                               SIZEOF_BTN_GROUP1)) {
    trace(" : SIMPLINK");
  }
}

void readThread(asio::serial_port &port,
                std::shared_ptr<circular_buffer> cb_ptr,
                std::shared_ptr<read_state> state = {}) {
  if (!state) {
    // shared state for entire async chain
    state = std::make_shared<read_state>();
  }

  port.async_read_some(
      asio::buffer(*state),
      [&port, cb_ptr, state](error_code ec, size_t bytes_read) {
        if (!ec) {
          if (static_cast<int>((*state)[0]) == 0) {
            matchButtons(cb_ptr);
            cb_ptr->clear();
          } else {
            cb_ptr->push_back(static_cast<int>((*state)[0]));

            if (static_cast<int>((*state)[0]) == 255 && cb_ptr->size() >= 6) {
              matchButtons(cb_ptr);
              cb_ptr->clear();
            }
          }
          readThread(port, cb_ptr, state);
        }
      });
}

int main(int argc, char *argv[]) {
  std::shared_ptr<circular_buffer> cb_ptr =
      std::make_shared<circular_buffer>(8);

  asio::thread_pool service;
  auto strand = make_strand(service);

  boost::asio::posix::stream_descriptor stdout(strand, ::dup(STDOUT_FILENO));

  using asio::serial_port;
  serial_port port(strand, default_device);

  asio::signal_set sig(service, SIGINT, SIGTERM);
  sig.async_wait([&](error_code ec, int num) {
    if (!ec) {
      post(strand, [&] { port.close(); });
    }
    service.stop();
  });

  port.set_option(serial_port::baud_rate(1200));
  port.set_option(serial_port::parity(serial_port::parity::none));
  port.set_option(serial_port::stop_bits(serial_port::stop_bits::one));
  port.set_option(serial_port::character_size(8));
  port.set_option(
      serial_port::flow_control(serial_port::flow_control::hardware));

  std::cout << "capturing... " << std::endl;
  readThread(port, cb_ptr);
  service.join();
  std::cout << std::endl << "done." << std::endl;
  port.close();
}
