project(
    'reburn',
    'cpp',
    version : '0.1.0',
    meson_version : '>= 0.56.0',
    default_options : ['cpp_std=c++17']
)

conf_data = configuration_data()
conf_data.set('version', meson.project_version())

compiler = meson.get_compiler('cpp')

compiler_flags = [
    '-Wall',
    '-fpermissive',
    '-fno-strict-aliasing'
]

linker_flags = [
    '-lm',
    '-latomic',
    '-lrt',
    '-lpthread'
]

boost_static_compile = false

configure_file(input : 'config.h.in',
               output : 'config.h',
               configuration : conf_data)
sources = [
    'main.cpp'
]

deps = [
    dependency('threads', required: true),
    dependency('openssl', required: true),
    dependency('boost', modules: [
                    'system',
                    'thread'
                ],
                required: true,
                version: '=1.82.0',
                static: boost_static_compile
    )
]

add_project_arguments(compiler.get_supported_arguments(compiler_flags), language : 'cpp')
add_project_link_arguments(linker_flags, language: 'cpp')

executable('reburn', sources, dependencies: deps)
